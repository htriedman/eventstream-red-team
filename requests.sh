#!/bin/bash
echo "[" >> schemas.json
while read l; do
  echo "$l"
  url="https://datahub-gms.discovery.wmnet:30443/openapi/entities/v1/latest?urns=urn:li:dataset:(urn:li:dataPlatform:hive,event."
  url+="$l"
  url+=",PROD)&aspectNames=schemaMetadata"
  echo "url: $url"
  out=`curl -X GET "$url" -H "accept: application/json"`
  echo "{\"$l\":$out}," >> schemas.json
done < tables.txt
sed -i '$ s/.$//' schemas.json
echo "]" >> schemas.json
